import sys

importlib = __import__('importlib')

COMMAND_MOD = sys.argv[1]

def main():
    act_comm_mod = importlib.import_module(f'loaders.{COMMAND_MOD}')
    act_comm_mod.command(*sys.argv[2:])

main()
