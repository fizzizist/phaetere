from typing import List, Union, Dict, Tuple
from markdown import markdown

import os

BLOG_DIR = "blog_posts"


def get_blog_list() -> List[str]:
    return os.listdir(BLOG_DIR)


def get_blog_post(filename: str) -> Dict[str, Union[str, List[Tuple[str, List[str]]]]]:
    """Retrieve a blog post from the database.

    :param post_id: The ID of the post to retrieve
    """
    filepath = os.path.join(BLOG_DIR, filename)

    if not os.path.exists(filepath):
        raise ValueError(f"{filepath} not found")

    with open(filepath) as f:
        content = markdown(f.read(), extensions=["fenced_code", "codehilite"])

        return {"post_content": content}
