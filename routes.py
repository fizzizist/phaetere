import os

from flask import Flask, render_template, Response
from blog import get_blog_list, get_blog_post

IMAGE_FOLDER = os.path.join("static", "images")

app = Flask(__name__)
app.config.update(IMAGE_FOLDER=IMAGE_FOLDER, SECRET_KEY=os.environ["SECRET_KEY"])


@app.route("/")
def index():
    me_filepath = os.path.join(app.config["IMAGE_FOLDER"], "me.jpg")
    return render_template("index.html", me=me_filepath)


@app.route("/contact")
def contact():
    return render_template("contact.html")


@app.route("/blog")
def blog():
    return render_template("blog.html", posts=get_blog_list())


@app.route("/blog/<filename>")
def blog_post(filename: str):
    try:
        post_data = get_blog_post(filename)
    except ValueError:
        return Response(
            response=f"404: No blog post found for file: {filename}", status=404
        )

    return render_template("blog_post.html", post_content=post_data["post_content"])


@app.route("/projects")
def project():
    return render_template("projects.html")
