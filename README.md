# Phaetere 2023.12.0

Phaetere is Peter Vlasveld's personal website with all of the usual stuff: portfolio, blog, etc.

## Blog

The blog is made of md files that are stored in a separate git repository. The repository needs to be git pulled into the `blog_posts` directory in order to see the blog posts.

## Environment

This app is meant to run in Docker, and uses docker profiles with docker compose. To develop, you should be able to just run `docker compose up`.

In prod, the .env file will be an altered version of `.env.dev`, and the prod profile will be used.
