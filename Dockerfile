# Builder

FROM python:3.10-bullseye as builder

MAINTAINER Fizzizist "pvlasveld@protonmail.com"

COPY ./requirements/prod.txt /app/requirements/prod.txt

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip wheel --no-cache-dir --no-deps --wheel-dir /app/wheels -r requirements/prod.txt

FROM python:3.10-bullseye as dev

MAINTAINER Fizzizist "pvlasveld@protonmail.com"

COPY ./requirements/base.txt /app/requirements.txt

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install pip-tools && pip-sync

COPY . .

FROM python:3.10-bullseye as prod

RUN apt update -y \
  && mkdir -p /home/phaetere \
  && groupadd phaetere \
  && useradd phaetere -s /bin/bash -g phaetere

ENV HOME=/home/phaetere
ENV APP_HOME=/home/phaetere/app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

COPY --from=builder /app/wheels /wheels
RUN pip install --no-cache /wheels/*

COPY . $APP_HOME

RUN chown -R phaetere:phaetere $APP_HOME

USER phaetere
